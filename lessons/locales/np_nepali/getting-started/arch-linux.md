# Arch Linux

## पाठ

<b>पृष्ठ</b>
Arch हल्का र लचिलो Linux distribution हो जुन पुरै open source समाजले चलाएको छ। Debian जस्तै, Archले rolling release model अपनाएको छ त्यसैले testing मा release भएका सिधै Stable release हुन्छन्। यसको system र function बुझ्न रिस्क लिई चलाई राख्नुपर्छ, त्यसको बदलामा तिमीले systemको पुरै अधिकार पाउछौ।

<b>Package (प्याकेज) व्यवस्थापक</b>
package install, update र manage(व्यवस्थापन) गर्न यसको आफ्नै package manager छ, Pacman। 

<b>लचक्ता</b>
हलुका operating system र Linux बुझ्न मन छ भने Arch चलाऊ! सिक्न समय लाग्छ, तर hardcore (खतरा) Linux userका लागि, यो राम्रो चोइस हो।

<b>भुमिका</b>
desktop र laptop लाई यो एकदम ठिक छ। Raspberry Pi जस्तो हलुका र सानो device छ अनि हालुका OS त्यसमा चाईएको छ भने , Arch लाई केहि भन्नैपर्दैन।

## अभ्यास

तिमीलाई Arch operating system तिम्रो होस् जस्तो लाग्छ भने , install सेक्सनतर्फ लाग : <a href='https://www.archlinux.org/'>https://www.archlinux.org/</a>

## Quiz प्र्श्न

Arch Linux ले कुन package managerको प्र्योग गर्छ?

## Quiz जवाफ

Pacman
