# मुख्य पृष्ठ

## Grasshopper

* Getting Started - Linux के हो? distribution चुन्न र install गर्नतर्फ लाग्नुहोस्।

* Command Line - command line को आधार जान्नुहोस्, फाईलहरुसँग खेल्न जान्नुहोस् , फोलडरहरु(directories) र अझ।

* Text-Fu - हल्काफुल्का अक्छर काटकुट पार्न र चलाउन सिक्नुहोस्।

* Advanced Text-Fu - vim र emacsको सहायताले अक्छर Linux माकुरे-बाँदर जसरी चलाउनुहोस्।

* User Management - userको भुमिका र व्यवस्थापनको बारेमा सिक्नुहोस्।

* Permissions - permission (अनुमति) तह र permissions फेर्न सिक्नुहोस्।

* Processes - systemमा चलिरहेका processes (कार्यहरु) का बारेमा सिक्नुहोस्।

* Packages - dpkg, apt-get, rpm र yum package management tools को बारेमा सिक्नुहोस्।

## Journeyman

* Devices - Linux devicesका बारेमा र ती कसरी kernel र user सँग काम गर्छन्, सिक्नुहोस्।

* The Filesystem - Linux filesystem, बिभिन्न किसिमका filesystems, partitioning र अरुका बारेमा सिक्नुहोस्।

* Boot the System - Linux boot processका तहका बारेमा बुझ्नुहोस्।

* Kernel - Linux system सभैभन्दा मुख्य भाग , ती कसरी चल्छन् र तिनलाई कसरी configure गर्ने सिक्नुहोस्।

* Init - SysV, Upstart र systemd जस्ता बिभिन्न init systemsका बारेमा बुझ्नुहोस्।

* Process Utilization - top, load averages, iostat र अरुले resource monitoring गर्न सिक्नुहोस्।

* Logging - system logs र /var/log directoryका बारेमा बुझ्नुहोस्।

## Networking Nomad

* Network Sharing - rsync, scp, nfs र अरुको मद्धतले गरिने network sharingका बारेमा बुझ्नुहोस्।

* Network Basics - networking basics र TCP/IP modelका बारेमा बुझ्नुहोस्।

* Subnetting - subnets र subnet arithmetic कसरी गर्ने सिक्नुहोस्।

* Routing - networkमा packets कसरी route गरिन्छ सिक्नुहोस्!

* Network Config - Linux toolsले कसरी network configuration गरिन्छ सिक्नुहोस्!

* Troubleshooting - diagnose र troubleshoot गर्न मद्धत गर्ने साधारण networking toolsका बारेमा बुझ्नुहोस्। 

* DNS - DNSका बारेमा तपाईलाई जान्न मन लागेका कुरा सबै र अझ धेरैका बारेमा सिक्नुहोस्।
