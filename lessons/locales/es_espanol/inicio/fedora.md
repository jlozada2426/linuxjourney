# Fedora



<b>Introducción</b>
Respaldado por Red Hat, el proyecto Fedora es desarrollado por la comunidad y contiene software gratuito y de código abierto. Red Hat Enterprise Linux se desprende de Fedora, asi que puedes pensar en Fedora como el sistema operativo padre de RHEL. Eventualmente RHEL obtendrá las actualizaciones de Fedora a traves de pruebas y control de calidad exhaustivo. Piensa en que Fedora es el equivalente a Ubuntu solo que se basa en Red Hat en vez de Debian.

<b>Administración de paquetes</b>
Utiliza el mismo administrador de paquetes que Red Hat.

<b>Configurabilidad</b>
Si quieres un sistema basado en Red Hat, esta es la versión accesible.

<b>Usos</b>
Fedora es genial si quieres un sistema operativo basado en Red Hat sin el precio comercial. Recomendado para computadoras de escritorio y laptops.

<li><a href='https://fedoraproject.org/wiki/Join'>Join Fedora</a></li>
<li><a href='https://docs.fedoraproject.org/en-US/Fedora/26/html/System_Administrators_Guide/index.html'> Fedora 26 SA docs</a></li>
