# Fedora

<b>Overview</b>
Backed by Red Hat, the Fedora Project is community driven containing open-source and free software. Red Hat Enterprise Linux branches off Fedora, so think of Fedora as an upstream RHEL operating system. Eventually RHEL will get updates from Fedora after thorough testing and quality assurance. Think of Fedora as an Ubuntu equivalent that uses a Red Hat backend instead of Debian.

<b>Package Management</b>
Latest fedora versions use DNF as the package manager. This will replace yum in the long run in RHEL and centos.

<b>Configurability</b>
If you want to use a Red Hat based operating system, this is a user friendly version.

<b>Uses</b>
Fedora is great if you want a Red Hat based operating system without the price tag. Recommended for desktop and laptop. Fedora also has more of the latest releases of software compared to RHEL and centos.
