# man

You can see the manuals for a command with the man command.
```
man ls
```

Man pages are manuals that are by default built into most Linux operating systems. They provide documentation about commands and other aspects of the system.

Try it out on a few commands to get more information about them.
