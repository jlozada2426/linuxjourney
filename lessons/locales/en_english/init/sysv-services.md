# System V Service

There are many command line tools you can use to manage Sys V services.

<b>List services</b>

```
service --status-all
```

<b>Start a service</b>
```
sudo service networking start
```

<b>Stop a service</b>
```
sudo service networking stop
```

<b>Restart a service</b>

```
sudo service networking restart
```

These commands aren't specific to Sys V init systems, you can use these commands to manage Upstart services as well. Since Linux is trying to move away from the more traditional Sys V init scripts, there are still things in place to help that transition.
