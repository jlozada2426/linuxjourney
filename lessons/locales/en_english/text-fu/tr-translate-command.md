# tr (Translate)


The tr (translate) command allows you to translate a set of characters into another set of characters. Let's try an example of translating all lower case characters to uppercase characters.

<pre>$ tr a-z A-Z
hello
HELLO</pre>

As you can see we made the ranges of a-z into A-Z and all text we type that is lowercase gets uppercased.
