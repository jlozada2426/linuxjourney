# yum and apt



Ah, the Batmans of package management, these systems come with all the fixins to make package installation, removal and changes easier, including installing package dependencies. Two of the most popular management systems is <b>yum</b> and <b>apt</b>. Yum is exclusive to the Red Hat family and apt is exclusively to the Debian family.

<b>Install a package from a repository</b>

```
Debian: $ apt install package_name
RPM: $ yum install package_name
```

<b>Remove a package</b>

```
Debian: $ apt remove package_name
RPM: $ yum erase package_name
```

<b>Updating packages for a repository</b>

It's always best practice to update your package repositories so they are up to date before you install and update a package.

```
Debian: apt update; apt upgrade
RPM: yum update
```

<b>Get information about an installed package</b>

```
Debian: apt show package_name
RPM: yum info package_name
```
